package regex;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * класс, который ищет и выводит числа с фиксированной и плавающей точкой
 *
 * @author И.А.Прокофьева
 */
public class Regex {
    public static void main(String[] args) {
        String string = "2.5 -5.78 плюс + +67 .8 9. .  +. 23.12e+10";
        Pattern pattern = Pattern.compile("([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W");
        Pattern pattern1 = Pattern.compile("([+-]?((\\d*\\.\\d+)[e][+-](\\d*))|((\\d+\\.\\d*) [e][+-](\\d*)))");
        out(regex(string, pattern, pattern1));
    }

    /**
     * метод реализует поиск чисел с фиксированной и плавающей точкой
     * @param string строка, в которой ищутся необходимые пользователю числа
     * @param pattern регулярное выражение для поиска числа с фиксированной точкой
     * @param pattern1 регулярное выражение для поиска числа с плавающей точкой
     * @return возращает массив чисел с фиксироавнной/плавающей точкой
     */
    public static ArrayList<String> regex(String string, Pattern pattern, Pattern pattern1) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> patterns = new ArrayList<>();
        System.out.print("Вам вывести числа с плавающей(1) или фиксированной(2) точкой? \nВведите цифру: ");
        int numb = scanner.nextInt();
        if (numb == 1) {
            Matcher matcher = pattern1.matcher(string);
            while (matcher.find()) {
                patterns.add(String.valueOf(Pattern.compile(matcher.group())));
            }
        }
        if (numb == 2) {
            Matcher matcher = pattern.matcher(string);
            while (matcher.find()) {
                patterns.add(String.valueOf(Pattern.compile(matcher.group())));
            }
        }
        return patterns;
    }

    /**
     * метод для вывода чисел
     * @param patterns массив чисел с фиксированной/плавающей точкой
     */
    public static void out(ArrayList<String> patterns) {
        for (String pattern : patterns) {
            System.out.println(pattern);
        }
    }
}