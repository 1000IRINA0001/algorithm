package identifier;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * класс, который находит идентификаторы, и выводит их уникальные значения
 *
 * @author И.А.Прокофьева
 */
public class FindIdentifir {
    public static void main(String[] args) {
        /**
         * fileReader() считывает текстовый документ
         * */
        String listLine = fileReader();
        Pattern pattern = Pattern.compile("[a-zA-Z_][a-zA-Z_0-9]*");

        ArrayList<String> template = find(pattern, listLine);
        /**
         * вывод всех найденных идентификаторов
         */
        System.out.println("   *No unique*");
        out(template);
        /**
         * вывод уникальных идентификаторов
         */
        System.out.println("\n   *Unique*");
        out((HashSet<String>) unique(template));
    }

    /**
     * метод считывает текстовый файл и и делает его мультистрокой
     * @return возвращает мультистроку
     */
    private static String fileReader() {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get("D:\\algorithms\\src\\algorithms\\Sorting.java"));
            /**
             * получение мультистроки
             */
            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }

    /**
     * метод ищет в файле идентификаторы
     * @param listline массив с мультистрокой
     * @return возращает массив с идентификаторами
     */
    public static ArrayList<String> find(Pattern pattern, String listline) {
        ArrayList<String> patterns = new ArrayList<>();
        Matcher matcher = pattern.matcher(listline);
        while (matcher.find()) {
            patterns.add(String.valueOf(Pattern.compile(matcher.group())));
        }
        return patterns;
    }

    /**
     * ищет в массиве с идентификаторами уникальные идентификаторы
     * @param template массив с идентификаторами
     * @return возращает массив с уникальными идентификаторами
     */
    private static Set<String> unique(ArrayList<String> template) {
        HashSet<String> patterns = new HashSet<>();
        patterns.addAll(template);
        return patterns;
    }

    /**
     * метод для вывода массива с идентификаторами
     * @param patterns  массив с идентификаторами
     */
    public static void out(ArrayList<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }

    /**
     * метод для вывода массива с уникальными идентификаторами
     * @param patterns  массив с уникальными идентификаторами
     */

    public static void out(HashSet<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }
}
