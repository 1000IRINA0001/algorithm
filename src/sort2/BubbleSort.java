package sort2;
import java.util.Arrays;
/**
 *класс, демонстрирующий сортировку чисел по возрастанию с помощью пузырьковой сортировки
 * @author И.А.Прокофьева
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {-1,-5,0,5,3};


        //  массив перед пузырьковой сортировкой
        System.out.println("массив перед пузырьковой сортировкой : " + Arrays.toString(arr));

        bubbleSort(arr);
        //  массив после пузырьковой сортировки
        System.out.println("массив после пузырьковой сортировки : " + Arrays.toString(arr));
    }

    /**
     *  метод пузырьковой сортировки
      */

    private static void bubbleSort(int[] arr) {
        int j;
        boolean flag = true;
        /**
         * первый проход по массиву: флаг = true
         */
        int temp;
        /**
         * вспомогательная переменная
         */
        while (flag) {
            flag = false;
            /**
             *  устанавливаем флаг в false (в ожидании , что два соседних числа в массиве поменяются местами)
             */
            for (j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    /**
                     *  меняем элементы местами
                     */
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    flag = true;
                }
            }
        }
    }
}

