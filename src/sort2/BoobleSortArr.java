package sort2;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *класс, демонстрирующий сортировку чисел по возрастанию с помощью пузырьковой сортировки
 * @author И.А.Прокофьева
 */

public class BoobleSortArr {
    public static void main(String[] args) {
        /**
         * создается ArrayList и заполняется числами
         */
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(2);
        numbers.add(-4);
        numbers.add(6);
        numbers.add(-8);
        numbers.add(0);
        numbers.add(1);
        numbers.add(9);
        numbers.add(7);
        System.out.print("массив до пузырьковой сортировки : " + numbers);
        System.out.println();
        BoobleSort(numbers);
        System.out.println("массив после пузырьковой сортировки : " + Arrays.toString(new ArrayList[]{numbers}));
    }

    /**
     * метод, в котором сортируются числа
     * @param numbers массив чисел
     */
    private static void BoobleSort(ArrayList<Integer> numbers) {
        int j;
        boolean flag = true;   //первый проход по массиву: флаг = true
        int temp;   // вспомогательная переменная

        while (flag) {
            flag = false;    // устанавливаем флаг в false (в ожидании , что два соседних числа в массиве поменяются местами)
            for (j = 0; j < numbers.size() - 1; j++) {
                if (numbers.get(j) > numbers.get(j + 1)) {
                    temp = numbers.get(j);         // меняем элементы местами
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, temp);
                    flag = true;
                }
            }
        }
    }
}

