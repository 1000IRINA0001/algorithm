package sort;

import java.util.Arrays;

/**
 *класс, демонстрирующий сортировку выбора по возрастанию
 * @author И.А.Прокофьева
 */
public class SortTheSelection {
    public static void main(String[] args) {
        int array[] = {-1,-5,-10,0,2,5};

        System.out.println("Массив до сортировки : "+Arrays.toString(array));

        System.out.println("Массив после сортировки: "+ Arrays.toString(SelectSort(array)));


    }

    /**
     *метод для сортировки чисел массива по пвозрастанию
     * @param array массив чисел
     * @return возвращаем отсортированный массив
     */
    private static int[] SelectSort(int array[]) {
        for (int i = array.length-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }


}