package sort;
/**
 *класс, демонстрирующий сортировку выбора по возрастанию
 * @author И.А.Прокофьева
 */
import java.util.ArrayList;

public class Sort {
    public static void main(String[] args) {
        /**
         * создается ArrayList и заполняется числами
         */
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(2);
        numbers.add(-4);
        numbers.add(6);
        numbers.add(8);
        numbers.add(-5);
        numbers.add(1);
        numbers.add(9);
        numbers.add(0);
        System.out.print("Массив до сортировки: " + numbers);
        System.out.println();
        System.out.println("Массив после сортировки: "+Sorting(numbers));
    }

    /**
     * метод сортирует числа по возрастанию
     * @param numbers массив чисел
     * @return возвращает массив с отсортированными числами
     */
    private static ArrayList<Integer> Sorting(ArrayList<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            for (int j = 0; j < numbers.size() - 1; j++) {
                if (numbers.get(j) > numbers.get(j + 1)) {
                    int temp = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, temp);
                }
            }
        }
        return numbers;
    }
}


