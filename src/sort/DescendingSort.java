package sort;

import java.util.Collections;
import java.util.Arrays;
/**
 *класс, демонстрирующий сортировку выбора по возрастанию в ArrayList с помощью метода Collections
 * @author И.А.Прокофьева
 */

public class DescendingSort {
        public static void main(String[] args) {
/**
 * создаем ArrayList и заполняем его значениями
 */Integer[] numbers = {3,4,1,5,2,8,7,9,6};
            Arrays.sort(numbers);
            System.out.println(Arrays.toString(numbers));
/**
 * сортировка чисел в ArrayList с помощью метода Collections
 * reverseOrder() - сортировка справа налево
 */
            Arrays.sort(numbers, Collections.reverseOrder());
            System.out.println(Arrays.toString(numbers));




        }
    }

