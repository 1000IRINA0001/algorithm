package sort;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 *класс, демонстрирующий сортировку выбора по возрастанию с помощью метода Collections
 * @author И.А.Прокофьева
 */
public class SortingTheSelection {
    public static void main(String[] args) {
/**
 * создаем ArrayList и заполняем его значениями
 */
        List<Integer> numbers = new ArrayList<>();
        numbers.add(-3);
        numbers.add(2);
        numbers.add(0);
        numbers.add(6);
        numbers.add(8);
        numbers.add(-5);
        numbers.add(1);
        numbers.add(9);
        numbers.add(7);
        System.out.print(numbers);
        Sort(numbers);
    }

    /**
     * сортирует числа в ArrayList
     * @param numbers массив чисел
     */
        private static void Sort(List<Integer> numbers){
            /**
             * метод сортирует числа от наименьшего к наибольшему
             */
        Collections.sort(numbers);
        System.out.print(numbers);
    }
}

