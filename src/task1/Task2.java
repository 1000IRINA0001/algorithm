package task1;

import java.util.ArrayList;
import java.util.Scanner;
/**
 *класс, демонстрирующий сортировку выбора по возрастанию с помощью метода Collections
 * @author И.А.Прокофьева
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число: ");
        int number = scanner.nextInt();
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(2);
        numbers.add(-4);
        numbers.add(6);
        numbers.add(8);
        numbers.add(-5);
        numbers.add(1);
        numbers.add(9);
        numbers.add(0);
        System.out.println("Массив : " + numbers);

        Number(numbers, number);
    }

    /**
     * метод ищет в массиве введенный элемент
     * @param numbers массив чисел
     * @param number введеное число
     */
    private static void Number(ArrayList<Integer> numbers, int number) {
        int index = 0;
        if (index == numbers.indexOf(number)) {
            System.out.println("Число в массиве найдено : " + index);
        } else {
            System.out.println("элемент не найден");
        }
    }
}

