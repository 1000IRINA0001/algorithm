package interfaces.readers;

import interfaces.readers.IReader;

public class PredefinedReader implements IReader {
    private String text; // берёт

    public PredefinedReader(String text) { // сохраняет
        this.text = text;
    }

    @Override
    public String read() { // возвращаем текст
        return text;
    }
}
