package interfaces;

import interfaces.printes.IPrinter;
import interfaces.readers.IReader;

class Replacer {
    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) { // получает и сохраняет у себя в полях
        this.reader = reader;
        this.printer = printer;
    }

    void replace() {
        final String text = reader.read();
        final String replacedText = text.replace(":(", ":)");//сменяет смайлики
        printer.print(replacedText);
    }
}
