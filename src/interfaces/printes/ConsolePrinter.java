package interfaces.printes;

public class ConsolePrinter implements IPrinter {

    @Override
    public void print(final String text) {
        System.out.println("***");
        System.out.println(text);
        System.out.println("***");
    } }
