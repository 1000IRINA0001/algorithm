package binarySearch;
/**
 *класс, демонстрирующий бинарный поиск в массиве и ArrayList
 * @author И.А.Прокофьева
 */

import java.util.*;

public class BinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int num = scanner.nextInt();
        int array[] = {3, 6, 1, 2, 7, 9, -5};
        System.out.print("Массив до сортировки: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        List<Integer> numbers = new ArrayList<>();
        numbers.add(-3);
        numbers.add(2);
        numbers.add(0);
        numbers.add(8);
        numbers.add(-5);
        numbers.add(1);

        System.out.print("array list до сортировки: " + numbers + "\n");
        System.out.println("Массив после сортировки: " + Arrays.toString(sort(array)));
        System.out.println("ArrayList после сортировки: " + sort(numbers));
        System.out.println();
        binary(array, num);
        binary(numbers, num);
    }

    /**
     * метод сортирует числа в массиве по возрастанию
     * @param array массив чисел
     * @return возвращает отсортированный массив
     */
    private static int[] sort(int[] array) {
        Arrays.sort(array);
        return array;
    }
    /**
     * метод сортирует числа в ArrayList по возрастанию
     * @param numbers массив чисел
     * @return возвращает отсортированный массив
     */
    private static List<Integer> sort(List<Integer> numbers) {
        Collections.sort(numbers);
        return numbers;
    }

    /**
     * метод ищет с помощью бинарного поиска число в массиве, введеное пользователем
     * @param array массив отсортированных чисел
     * @param num введенное пользователем число
     * @return возвращает результат: число найдено/не найдено
     */
    private static int binary(int[] array, int num) {
        int first = 0;
        int last = array.length - 1;

        while (first <= last) {
            int mid = (last + first) / 2;
            int g = array[mid];
            if (g == num) {
                System.out.println("Число " + "[" + num + "]" + " найдено в массиве по индексу " + mid);
                return mid;
            }
            if (g < num) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }

        }
        System.out.println("Число " + "[" + num + "]" + " в массиве  не найдено");
        return -1;
    }
    /**
     * метод ищет с помощью бинарного поиска число в ArrayList, введеное пользователем
     * @param numbers массив отсортированных чисел
     * @param num введенное пользователем число
     * @return возвращает результат: число найдено/не найдено
     */
    private static int binary(List<Integer> numbers, int num) {

        int first = 0;
        int last = numbers.size() - 1;

        while (first <= last) {
            int mid = (last + first) / 2;
            int g = numbers.get(mid);
            if (g == num) {
                System.out.println("Число " + "[" + num + "]" + " найдено в ArrayList по индексу: " + mid);
                return mid;
            }
            if (g < num) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }

        }
        System.out.println("Число " + "[" + num + "]" + " в ArrayList не найдено");
        return -1;
    }
}

