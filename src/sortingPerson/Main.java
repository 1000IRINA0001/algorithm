package sortingPerson;
/**
 * класс, демонстрирующий сортировку объектов:
 * Сортировка в алфавитном порядке
 * Сортировка по годам рождения
 * Поиск по фамилии
 *
 * @author И.А.Прокофьева
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Ершов", 2001);
        Person person2 = new Person("Волков", 2008);
        Person person3 = new Person("Лебедев", 2003);
        Person person4 = new Person("Волков", 2005);
        Person person5 = new Person("Иванов", 2004);

        ArrayList<Person> persons = new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);
        System.out.println(persons);
        System.out.println(SortSurname(persons));
        System.out.println((Age(persons)));
        System.out.println(SearchByLastName(persons));
    }

    /**
     * Метод сортирует объекты в алфавитном порядке с помощью сортировки выбором
     * @param persons массив объектов
     * @return возвращает отсортированный массив объектов
     */
    private static ArrayList<Person> SortSurname(ArrayList<Person> persons) {
        Person temp;
        for (int i = 0; i < persons.size(); i++) {
            for (int j = 1 + i; j < persons.size(); j++) {
                int compare = persons.get(j).getName().compareTo(persons.get(i).getName());
                if (compare < 0) {
                    temp = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, temp);
                }
                //если в массиве несколько объектов с одинаковой фамилией, то эти объекты сортируются по годам рождения(по возрастанию)
                if (compare == 1) {
                    if (persons.get(j).getAge() < persons.get(i).getAge()) {
                        temp = persons.get(i);
                        persons.set(i, persons.get(j));
                        persons.set(j, temp);
                    }
                }
            }
        }
        return persons;
    }

    /**
     * Метод сортирует объекты по годам рождения(по убыванию) с помощью сортировки выбором
     * @param persons массив объектов
     * @return возращает отсортированный массив
     */
    private static ArrayList<Person> Age(ArrayList<Person> persons) {
        Person temp;
        for (int i = 0; i < persons.size(); i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                if (persons.get(j).getAge() > persons.get(i).getAge()) {
                    temp = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, temp);
                }
            }
        }
        return persons;
    }

    /**
     * Метод проверяет введенную пользователем фамилию и выводит данные об объекте(объектах), если такая фамилия есть в объекте(объектах)
     * @param persons массив объектов
     * @return возращает новый массив, если нашел объект с введенной фамилией. Если нужная фамилия в объектах не найдена, выведется пустой массив.
     */
    private static ArrayList<Person> SearchByLastName(ArrayList<Person> persons) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Person> people = new ArrayList<>();
        System.out.println("Введите фамилию: ");
        String Surname = scanner.nextLine();
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getName().equals(Surname)) {
                people.add(persons.get(i));
            }
        }
        return people;
    }
}





