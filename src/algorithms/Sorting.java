package algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sorting {
    public static void main(String[] args) {
        int[] array = {4, 2, 5, -2, 0};
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(-3);
        arr.add(2);
        arr.add(0);
        arr.add(6);
        arr.add(8);
        arr.add(-5);
        arr.add(1);
        arr.add(9);
        arr.add(7);
        System.out.println("Массив перед сортировкой: " + Arrays.toString(array));
        insertionSort(array);
        System.out.println("Массив после сортировки: " + Arrays.toString(array));
        System.out.println("массив перед сортировкой с ArrayList : " + Arrays.toString(new List[]{arr}));
        insertionSort(arr);
        System.out.println("массив после сортировки c ArrayList : " + Arrays.toString(new List[]{arr}));
    }

    private static void insertionSort(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array[out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;

            }
            array[in] = temp;
        }

    }

    private static void insertionSort(ArrayList<Integer> arr) {
        for (int out = 1; out < arr.size(); out++) {
            int temp = arr.get(out);
            int in = out;
            while (in > 0 && arr.get(in - 1) <= temp) {
                arr.set(in, arr.get(in - 1));
                in--;
            }
            arr.set(in, temp);
        }
    }
}
