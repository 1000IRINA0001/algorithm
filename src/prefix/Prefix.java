package prefix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * класс, который ищет слова с приставками при- и пре-
 *
 * @author И.А.Прокофьева
 */
public class Prefix {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|Пре|При)[а-яё]+\\b");
        Matcher matcher = pattern.matcher("");

        try (BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ASUS\\Desktop\\input.txt"))) {
            while ((s = reader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                }
            }
        }
    }
}